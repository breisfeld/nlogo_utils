# add functions to namespace

from .combine import combine_nlogo_sections
from .split import split_nlogo_file
from .extract import extract_nlogo_section
from .replace import replace_nlogo_section