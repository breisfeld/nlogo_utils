# Instructions for deploying package to PyPI

0. If needed, examine authentication and account info in `~/.pypirc`

1. Make sure that the following are installed: `build`, `setuptools`, `wheel`, and `twine`

2. Update `version` in `setup.cfg`, for example,
   ```
   version = 0.1.2
   ```

3. Provide a matching tag for `version` in git, for example,

   ```
   git tag -a v0.1.2 -m "Add tag for new release to PyPI"
   git push --tags
   ```

4. Build the distribution files:

   ```
   $ python -m build
   ```

5. Deploy to test server

   ```
   $ twine upload --verbose --repository testpypi dist\*
   ```

6. Deploy to production server

   ```
   $ twine upload --verbose --repository pypi dist\*
   ```
