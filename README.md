# nlogo_utils

- [Purpose](#purpose)
- [Python API](#python-api)
  - [Split a .nlogo file and store the resulting section files in a specified directory](#split-a-nlogo-file-and-store-the-resulting-section-files-in-a-specified-directory)
  - [Extract a specified section from a .nlogo file and store it in a separate file](#extract-a-specified-section-from-a-nlogo-file-and-store-it-in-a-separate-file)
  - [Write a new .nlogo file, replacing a section of a source .nlogo file with the contents of a specified section file](#write-a-new-nlogo-file-replacing-a-section-of-a-source-nlogo-file-with-the-contents-of-a-specified-section-file)
  - [Assemble sections from specified directories into a .nlogo file](#assemble-sections-from-specified-directories-into-a-nlogo-file)
- [Command line](#command-line)
  - [Usage](#usage)
  - [Examples](#examples)
    - [Split a .nlogo file into component sections](#split-a-nlogo-file-into-component-sections)
    - [Extract a section from a .nlogo file](#extract-a-section-from-a-nlogo-file)
    - [Replace a section from the contents of an existing .nlogo file and create a new file](#replace-a-section-from-the-contents-of-an-existing-nlogo-file-and-create-a-new-file)
    - [Combine component sections from a single directory into a .nlogo file](#combine-component-sections-from-a-single-directory-into-a-nlogo-file)
    - [Combine component sections from various directories into a .nlogo file](#combine-component-sections-from-various-directories-into-a-nlogo-file)
- [Notes](#notes)

## Purpose

Functions and command-line utilities to manipulate and create native NetLogo (.nlogo) files.

Information about the NetLogo project itself may be found [here](https://ccl.northwestern.edu/netlogo/).


## Python API

Current functionality is as follows:

### Split a .nlogo file and store the resulting section files in a specified directory

```
split_nlogo_file(nlogo_file_path, sections_dir=None)

Arguments:
  nlogo_file_path: path to source nlogo file
  sections_dir: path to directory in which to store section files
```

### Extract a specified section from a .nlogo file and store it in a separate file
```
extract_nlogo_section(nlogo_file_path, section_name, section_file_path=None)

Arguments:
  nlogo_file_path: path to source nlogo file
  section_name: name of the section of the file to be extracted
  section_file_path: path to store the file created from the nlogo section
```

### Write a new .nlogo file, replacing a section of a source .nlogo file with the contents of a specified section file
```
replace_nlogo_section(nlogo_file_path, section_name, section_file_path, output_file_path)

Arguments:
  nlogo_file_path: path to source nlogo file
  section_name: name of the section of the file to be replaced
  section_file_path: path to the section file to be used as the replacement
  output_file_path: path to the nlogo file containing the replaced section
```

### Assemble sections from specified directories into a .nlogo file
```
combine_nlogo_sections(sections_dir, nlogo_file_path, other_section_files=None)

Arguments:
  sections_dir: path to directory from which to retrieve section files
  nlogo_file_path: path to target nlogo file
  other_section_files: a dictionary containing a mapping between the section name and location of the corresponding section file
```

## Command line

Current command-line utilities include the following:

`split_nlogo_file`: Split a .nlogo file and store the resulting section files in a specified directory

`extract_nlogo_section`: Extract a specified section from a .nlogo file and store it in a separate file

`replace_nlogo_section`: Write a new .nlogo file, replacing a section of a source .nlogo file with the contents of a specified section file

`combine_nlogo_sections`: Assemble sections from specified directories into a .nlogo file

### Usage

```
$ split_nlogo_file -h

usage: split_nlogo_file [-h] -i INPUT_FILE [-o OUTPUT_DIR]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_FILE, --input-file INPUT_FILE
                        path to .nlogo file
  -o OUTPUT_DIR, --output-dir OUTPUT_DIR
                        path to .nlogo section directory
```

```
$ extract_nlogo_section -h

usage: extract_from_nlogo [-h] -i INPUT_FILE -s SECTION_NAME [-o OUTPUT_FILE]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_FILE, --input-file INPUT_FILE
                        path to .nlogo file
  -s SECTION_NAME, --section-name SECTION_NAME
                        name of the .nlogo section to be extracted. valid names are 'code', 'interface', 'info',
                        'turtleshapes', 'version', 'previewcommands', 'systemdynamics', 'behaviorspace', 'hubnetclient',
                        'linkshapes', 'modelsettings', 'deltatick'
  -o OUTPUT_FILE, --output-file OUTPUT_FILE
                        path to .nlogo section file
```

```
$ replace_nlogo_section -h

usage: replace_nlogo_section [-h] -i INPUT_FILE -n SECTION_NAME -s SECTION_FILE -o OUTPUT_FILE

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_FILE, --input-file INPUT_FILE
                        path to .nlogo file
  -n SECTION_NAME, --section-name SECTION_NAME
                        name of the .nlogo section to be replaced. valid names are 'code', 'interface', 'info',
                        'turtleshapes', 'version', 'previewcommands', 'systemdynamics', 'behaviorspace', 'hubnetclient',
                        'linkshapes', 'modelsettings', 'deltatick'
  -s SECTION_FILE, --section-file SECTION_FILE
                        path to .nlogo replacement section file
  -o OUTPUT_FILE, --output-file OUTPUT_FILE
                        path to .nlogo file with replaced section
```

```
$ combine_nlogo_sections -h

usage: combine_nlogo_sections [-h] -i INPUT_DIR -o OUTPUT_FILE [--code-file CODE_FILE] [--interface-file INTERFACE_FILE]
                              [--info-file INFO_FILE] [--turtleshapes-file TURTLESHAPES_FILE] [--version-file VERSION_FILE]
                              [--previewcommands-file PREVIEWCOMMANDS_FILE] [--systemdynamics-file SYSTEMDYNAMICS_FILE]
                              [--behaviorspace-file BEHAVIORSPACE_FILE] [--hubnetclient-file HUBNETCLIENT_FILE]
                              [--linkshapes-file LINKSHAPES_FILE] [--modelsettings-file MODELSETTINGS_FILE]
                              [--deltatick-file DELTATICK_FILE]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input-dir INPUT_DIR
                        path to directory of .nlogo section files
  -o OUTPUT_FILE, --output-file OUTPUT_FILE
                        path to the output .nlogo file
  --code-file CODE_FILE
                        path to the code section file
  --interface-file INTERFACE_FILE
                        path to the interface section file
  --info-file INFO_FILE
                        path to the info section file
  --turtleshapes-file TURTLESHAPES_FILE
                        path to the turtleshapes section file
  --version-file VERSION_FILE
                        path to the version section file
  --previewcommands-file PREVIEWCOMMANDS_FILE
                        path to the previewcommands section file
  --systemdynamics-file SYSTEMDYNAMICS_FILE
                        path to the systemdynamics section file
  --behaviorspace-file BEHAVIORSPACE_FILE
                        path to the behaviorspace section file
  --hubnetclient-file HUBNETCLIENT_FILE
                        path to the hubnetclient section file
  --linkshapes-file LINKSHAPES_FILE
                        path to the linkshapes section file
  --modelsettings-file MODELSETTINGS_FILE
                        path to the modelsettings section file
  --deltatick-file DELTATICK_FILE
                        path to the deltatick section file
```

### Examples

#### Split a .nlogo file into component sections

```
$ split_nlogo_file -i 'Wolf Sheep Predation.nlogo'
```

This creates a directory of section files called `Wolf Sheep Predation.sections` in the same parent directory as the input .nlogo file.

If the default directory path is not preferred, the pathname of this directory can be specified using the '-o' option.

---

#### Extract a section from a .nlogo file

```
$ extract_nlogo_section -i 'Wolf Sheep Predation.nlogo' -s 'code'
```

This creates a file 'Wolf Sheep Predation.code.nlsec' in the same directory as the .nlogo file.

If the default pathname is not preferred, the pathname of the output section file can be specified using the '-o' option.

---

#### Replace a section from the contents of an existing .nlogo file and create a new file

```
$ replace_nlogo_section.exe -i Fire.nlogo -n code -s ./new_nlogo_sections/code.nlsec -o 'New Fire.nlogo'
```

---

#### Combine component sections from a single directory into a .nlogo file

```
$ combine_nlogo_sections.exe -i 'Wolf Sheep Predation.sections' -o wolf_sheep_predation.nlogo
```

---

#### Combine component sections from various directories into a .nlogo file

```
$ combine_nlogo_sections.exe -i 'Wolf Sheep Predation.sections' -o wolf_sheep_predation.nlogo --code-file ~/wsp1/code.nlsec --modelsettings-file ~/wsp2/modelsettings.nlsec --info-file ~/info_files/wsp_info.nlsec
```
---

## Notes

The line endings of output files will be those of the platform OS on which the utilities are run, regardless of those of the source files.
